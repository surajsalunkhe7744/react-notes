### Prop Drilling
* Prop drilling refers to the transmission of data or state through several component heirarchies.
* The act of transmitting data or state from a parent component to a child component, from a child component to another child component, and so forth, is referred to.
* Low code maintainability results from prop drilling.
* CodeSandBox - https://codesandbox.io/s/prop-drilling-hrd56v
* In the provided CodeSandBox example, the ParentComponent passes a message to Child1, who then passes it to Child2, Child2, Child3, Child3, Child4, and ultimately Child4 displays it. In this illustration, Child1, Child2, and Child3 did not need the message; but, in order to send the message to Child4, we must carry it from the parent, which is the prop-drilling.

### Context
* Context aids in resolving the prop drilling problem.
* This API provides a way to share data between two components without prop drilling.
* Using Context API, we can create context object which will hold the data for us.
* Then, without sending it to all of its subsequent child components, we can pass this data object to the child that needs it.

![context API and prop drilling](https://www.carlrippon.com/static/0d1f722d0fe4c2bc4c3d71595dbe67dd/ca682/prop-drilling-v-context.png)
* We could use context API to share use object between components.
```
import { createContext } from "react";

export const userContext = createContext({
	username: Mark
	email: mark@gmail.com
});
```

* Wrap the components in the provider component that provide userContext
```
export function User(props) {
const user = createContext({
	username: Mark
	email: mark@gmail.com
});

return (
	<userContext.Provider value={user}>
		<Header />
		<Main />
	</userContext.Provider>
);
}
```

* use the useContext() hook to access user component in your child component.
```
import { useContext } from "react";

export function Header() {
	const { username } = useContext(userContext);
	return <h2>{username}</h2>;
}
```

* CodeSandBox - https://codesandbox.io/s/exa-context-api-2dhf7z
* The components developed for the CodeSandBox example above are
ParentComponent - Export for userContext is contained in ParentComponent as per usual, and ParentComponent gives userContext to Child1 component having one input which updates the username.
Component for Child2 being rendered by Child1.
rendering of the Child3 component by Child2.
Child3 is rendering the component for Child4.
Child4: Using the hook useContext, rendering the username that was taken from userContext.
* You can see the logs for each component after changing the username using the input form supplied, and it is re-rendering after username state updation. So, here is the undesirable rendering that the Context API has brought to light.
* ref - https://dev.to/codeofrelevancy/what-is-prop-drilling-in-react-3kol

### Redux
* Redux provides a store that may be used to retrieve and change application state within the component.
* To implement the redux, we must write a store, actions, and reducers.
* **Actions** - Actions are used to convey data from an application to a redux store; they essentially consist of an action type and optional payload property, the payload of which contains updated data that has to be updated in the redux store. The action type helps the action accomplish the intended operation.
* **Reducers** - This function returns the updated state in accordance with the action and takes the current state and action type as arguments.
* **Store** - The store is holding the application state.
* Provider - The Provider component from React-redux makes the redux store accessible to the rest of the application.
```
<Provider store={store}>
 <App />
</Provider>
```
* Hooks - React-redux provides us hooks to communicate with the redux store
useSelector : Reads value from the store state.
```
import { useSelector } from "react-redux";

const counter = useSelector((state) => state.count);
```
useDispatch : Returns the store dispatch method to let you dispatch action.
```
import { useDispatch } from "react-redux";

import { incrementCounter } from "./action/incrementCounter";

// inside the functional component
const dispatch = useDispatch();
dispatch(incrementCounter);
```
CodeSandBox example to demonstrate basic use of above hooks  - https://codesandbox.io/s/exa-react-redux-q4jtmp

### Redux
* **Redux is predictable state container for javascript app**.
* Can be used with any UI library like Angular, React, Vue or even vanilla javascript.
* Redux is library for javascript applications.
* Redux stores state of our application., i.e. state which is shared to all the component's that are present in that application.
```
// LoginForm Component State
state = {
	username: "",
	password: "",
}
```
```
// UserList State
state = {
	users: []
}
```
```
// Application State
state={
isUserLoggedIn: false,
username: "Vishwas",
profileUrl: "",
onlineUsers: []
}
```
* In above snippets LoginForm and the UserList and the states of the react component but if we use the redux then our state will look like the Application State.
* State of the application can change., In redux pattern is enforced to ensure all state transactions are explicit and can be tracked.
* Why Redux ?
If we want to manage global state of our application and should be predictable way.
If our codebase is medium or large in size and might be worked on by many people.
If our application state is large and need to share the data in many components inside the application.
If app state is updating frequently over time.
If the logic of updating the state in getting complex.
* **Redux-Toolkit is official, opinionated, batteries included toolset for efficient redux development.**
* Why Redux-Toolkit
To overcome the redux shotcomings - 
1.Configure redux in an app seems complicated.
2.Addition to redux lot's of other packages we have to install to get redux to do something useful., packages like Redux-Thunk, Immer, Redux-Devtool
3.Redux require too much boilerplate code.
Redux-Toolkit serves as an abstraction over the redux. It hides the difficult part and ensuring good developer experience.
* React and Redux both works independently, react-redux is a official library which helps to bind two libraries.
* Three core concepts of redux - store, actions, reducer.
* Store that holds the state of our application.
* Action describes what happen in our application.
* Reduces which handles the action and decides how to update the state.
* Three Principles
1.The global state of our application is stored inside a single store as a single object.
2.The only way to change the state is dispatch an action., an object that describes what happen., In redux state is read only i.e. not allow directly to update the state.
3.Specify how the state tree is updated based on actions, we have to write pure reducers. Pure reducers are the pure functions which is taking previous state and the action as parameters and returns the new state.
![redux-workflow](https://velog.velcdn.com/images/sujeongji/post/27585444-0bb1-46a5-b33f-cad460b98bff/image.png)* Actions
It is the only way our application can communicate with the store.
It carries information in the form of plain javascript object from app to the store.
Having type property which describes what happened in the application.
Type property is defined string. 
```
const CAKE_ORDERED = CAKE_ORDERED;

function orderCake() {
	return {
		type: "CAKE_ORDERED",
		quantity: 1
	}
}
```
* Reducers
Specify how the application state is changing in response to action send to the store.
```
const initialState = {
numOfCakes: 10,
anotherProduct: 0
}

const reducer = (state = initialState, action) => {
switch(action.type){
	case "CAKE_ORDERED":
		return {
			...state, // As store contain more that one field need to spread state first.
			numOfCakes: state.numOfCakes - 1
		}
	}
	case default:
		return state;
}
```
* Store
Hold application state.
Allow access to state via getState()
Allow state to be updated via dispatch(action)
Registers listeners via subscribe(listeners)
Handle unregistering of listeners via the function retured by subcribe(listeners)
```
const store = createStore(reducer);

console.log("Application State :::", store.getState());

// subscribe will run on every update of the state.
const unsubscribe = store.subscribe(() => console.log("Updated State :::", store.getState()));

store.dispatch(orderCake());

unsubscribe();
```
* Bind Action Creators
Alternative way to dispatch the actions.
It is not that really necessary.
```
const actions = bindActionCreators({orderCake}, store.dispatch);
actions.orderCake();
```
* Multiple Reducers
Having more than one reducers in our codebase.
As here we split our state and reducer, it is useful to maintain code and helps to keep things seperate with responsibilities.
```
const initialCakeState = {
numOfCakes: 10,
}

const cakeReducer = (state = initialCakeState, action) => {
switch(action.type){
	case "CAKE_ORDERED":
		return {
			...state, // As store contain more that one field need to spread state first.
			numOfCakes: state.numOfCakes - 1
		}
	}
	case default:
		return state;
}

const initialMilkbarState = {
	numOfMilkbar: 10
}

const milkbarReducer = (state = initialMilkbarState, action) => {
switch(action.type){
	case "MILKBAR_ORDERED":
		return {
			...state, // As store contain more that one field need to spread state first.
			numOfMilkbar: state.numOfMilkbar - 1
		}
	}
	case default:
		return state;
}
```
* Combined Reducers
Redux provide method combineReducers to combine multiple reducers which can later can be passed to createStore methid.
```
const rootReducer = combineReducers({
	cake: cakeReducer,
	milkbar: milkbarReducer
});

const store = createStore(rootReducer);
```
* **Immer Library**
Installation - `npm i immer`
While writing logic for return new state in reducer, we have to spread the state if we have more than one fields in the state.
The scenarios mention in above code snippet seems ok but at that when we have to deal with big state it might be get tadeous.
```
// Normal way of updating the state

const UPDATE_CITY = UPDATE_CITY;
const updateCity = (cityName) => {
	return {
		type: "UPDATE_CITY",
		payload: cityName
	}
}

const initialState = {
	name: "Vishwas",
	address: {
		street: "123 main st",
		city: "Bostan",
		state: "MA"
	}
}

const reducer = (state = initialState, action) => {
	switch(action.type) {
		case "UPDATE_CITY":
			return {
				...state, // spread state to keep name field untouch
				address: {
					...state.address, // spread address to keep street and state field untouch
					city: action.payload
				}
			}
		case default:
			return state;
	}
}

const store = createStore(reducer);

const unsubscribe(() => console.log(store.getState()));

store.dispatch(updatCity("Dallas"));

unsubscribe();
```
After installation of Immer library it provides us two produce function which accepts currentState and callback which helps us to update the state in simple way.
```
// Below is reducer switch part after using the immer library

	const reducer = (state = initialState, action) => {
	switch(action.type) {
		case "UPDATE_CITY":
			return produce(state, (user) => {
				user.address.city = action.payload
			})
		case default:
			return state;
	}
}
```
* Middleware
Middleware helps us to add custom functionality between dispatching action and the moment it reaches to the reducer.
Middleware is useful for logging, crash reporting, performing asynchronous task etc.
redux provides us function applyMiddleware() which helps us to add middleware in redux, We pass pass it as argument to the store and need to pass arguments of middleware to applyMiddleware.
```
const store = createStore(reducer, applyMiddleware(logger));
// As per logger we can add many middleware's in redux for tht we just need to add argumnts in the applyMiddleware function.
```

### Redux Toolkit
* Action and Reducer - In redux toolkit actions and reducer are merged in a single file called slice. createSlice from redux-toolkit package helps us to create the slice.
* createSlice function have object as argument which contains three key and value pairs, name for slice, initial state and reducers.
* In redux toolkit we don't have define action types as keys mention in below snippet reducers are refered as action type.
* We have to export reducer and action from the slice.
* As we have written action and reducer in one file and also in optimized way we skipped boilerplate code which we have to write while using redux.
```
const initialState = {
	noOfCakes: 10
}

const cakeSlice = createSlice({
	name: "cake",
	initialState,
	reducers: {
		ordered: (state) => {
			state.noOfCakes--;
		}
		restocked: (state, action) => {
			state.noOfCakes += state.noOfCakes;
		}
	}
});
```
* Store -The store is created using configureStore() in the redux-toolkit. Redux's combineReducers() function must be used separately to support multiple reducers, but configureStore() in the redux-toolkit takes care of this for us.
```
const store = configureStore({
	reducer: {
		cake: cakeReducer,
		icereame: icereameReducer
	}
});
```
* Middleware - To add middleware in redux-toolkit, add "middleware" key with callback (helps to add middlewares) inside of the configureStore object.
We have to pass getDefaultMiddleware function as a argument and need to specify concat after that for add middleware's.
If we have to add more than one middlewares we have to pass them as a argument inside the concat.
```
const logger = createLogger();
const store = configureStore({
  reducer: {
    cake: cakeReducer,
    icecreame: icecreameReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)
});
```
* Extra Reducers - The interdependence of one slice reducer operation on another is a condition that extra reducers assist us manage.
```
// We have written snippet of cakeSlice above and suppose we have icecreameSlice which will getting free after each cake order. Here extra reducer comes in picture to help us.

onst initialState = {
	numOfIcecreames = 20;
}
const icecreameSlice = createSlice({
	name: "icecreame",
	initialState,
	reducers: {
		ordered: (state) => {
			state.numOfIcecreames--;
		},
		restocked: (state, action) => {
			state.numOfIcecreames += action.payload;
		}
	},
	extraReducers: (builder) => {
		builder.addCase(cakeAction.ordered, (state) => {
			state.numOfIcecreames--;
		})
	}
})
```
```
// We can write extraReducer by following method also but method mention in above snippet is recommended.

extraReducers: {
	["cake/ordered"]: () => {
		state.numOfIcecreames--;
	}
}
```
* Provider - Provider helps us to give access of our redux store to the application.
We have to install react-redux which provides us the Provider component and some hooks which helps us to access state which is hold by the store and to dispatch the actions.
```
import { Provider } from "react-redux";

// Inside return
<Provider store={store}>
    <App />
</Provider>
```
* useSelector Hook - useSelector hook helps us to access the data stored in the redux store.
```
import { useSelector } from "react-redux";

const numOfCakes = useSelector((state) => state.cake?.numOfCakes);
  return (
    <div>
      <h2>Num of Cakes - {numOfCakes}</h2>
      <button>Order Cake</button>
      <button>Restocked Cake</button>
    </div>
  );
```
* useDispatch Hook - useDispatch hook helps us to dispatch the action which cause the change in the store state.
```
import { ordered, restocked } from "./cakeSlice";

// Inside function
const numOfCakes = useSelector((state) => state.cake.numOfCakes);
const dispatch = useDispatch();

return (
	 <div>
	     <h2>Num of Cakes - {numOfCakes}</h2>
	     <button onClick={() => dispatch(ordered())}>Order Cake</button> // dispatching order
	     <button onClick={() => dispatch(restocked(5))}>Restocked Cake</button> // dispatching restocked
	 </div>
);
```
* CodeSandBox - https://codesandbox.io/s/exa-redux-toolkit-mts6yt
This CodeSandBox link contains above snippets implementation.


