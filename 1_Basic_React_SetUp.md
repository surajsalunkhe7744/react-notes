### Bable, Bundler and Build
* Javascript is translated using Babel.
* The react code won't be understood by browsers; however, bable can assist in converting the react code to plane javascript, which can subsequently be executed on browsers.
* Bundler aids in the bundling process, which involves merging many imported files into a single file.
* Bundler facilitates quicker browser execution and loading. Example: Webpack

![Bundeling](https://miro.medium.com/v2/resize:fit:1400/1*SVyedUtyXmpDcJaXuiZNRg.png)
* Babel and the bundler are included for us when we use the "create react app" command.
* Build is a packed version of our React code, which speeds up execution and decreases loading time.
