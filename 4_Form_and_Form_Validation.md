## Form and Form Validation

### Understanding basic HTML form without library

* HTML form for user data collection
* The HTML form is created using the form element, which also specifies all input components.
```  
<form>
// Form elements
</form>
```

Form Attributes :::
* action attribute - When a form is submitted, an action is defined by the action attribute.
* target attribute - When a form is submitted, the target attribute specifies where the response will appear.
target attribute values -
_blank: The response will appear in a new tab or window.
_self: It is the default setting that the response will display in the same window or tab.
_parent: The reply is going to show up in the parent frame.
_top: The entire body of the window will display the response.
framenames: The response is displayed in named frame.

(https://stackoverflow.com/questions/18470097/difference-between-self-top-and-parent-in-the-anchor-tag-target-attribute)

Form Elements :::
* The HTML form element contain following one or more elements -
```  
<input>
<label>
<select>
<textarea>
<button>
<fieldset>
<legend>
<datalist>
<output>
<option>
<optgroup>
```

#### The Input Element :::

* The input element is one of the most often utilized form elements.
* The input element is shown in accordance with the value of its type attribute.

Following are the different input type we can use in the HTML -
```
// defines a button
<input type="button">
```
```
// defines the checkbox
// Using a checkbox, a user can choose one or more alternatives from a small list of possibilities.
<input type="checkbox">
```
```
// used for input field that should contain a color
// depending on the browser support, the a colour picker will be displayed once we click on the color type input
<input type="color">
```
```
// used for input field that should contain the date
// depending on the browser date picker pop-up will be shown up
<input type="date">
```
```
// used for input field that should contain both date and time
// depending on the browser date picker pop-up will be shown up
<input type="datetime-local">
```
```
// used for input field that should contain email address
// depending on the browser email address will automatically validated.
<input type="email">
```
```
// defines file select field and provides "Browse" button to upload the file 
<input type="file">
```
```
// defines hidden input field which is not visible to the user, but user can edit it using browser developer tool.
<input type="hidden">
```
```
// defines image as a input button
<input type="image">
```
```
// allow user to select month and the year
<input type="month">
```
```
// defines the numeric input field
<input type="number">
```
```
// defines password field (Hidden content with dots)
<input type="password"> 
```
```
// defines a radio button,
// radio button let's user to select one of the limited number of choices
<input type="radio">
```
```
// defines a control for entering the number
// default range is 0 to 100
// we can set the restrictions using min, max and step.
<input type="range">
```
```
// defines a button to reset the form data to it's default value
<input type="reset">
```
```
// used for search field (Behaves like a regular text field)
<input type="search">
```
```
// defines a button to submit the form-data to form-handler
<input type="submit">
```
```
// used for text field that should contaiun the telephonic number
<input type="tel">
```
```
// defines single line input text field
<input type="text"> 
```
```
// allows user to select the time (without time zone)
// depending on the browser support, the time picker can show up in the input field.
<input type="time">
```
```
// used for input field that should contain the url
// depending on browser support, the url field will automatically validated when submitted.
<input type="url">
```
```
// allow user to select week and year
// depending on the browser support, a date picker can be show up in the input field. 
<input type="week">
```

#### The Label Element :::

* For form elements, the label tag defines label.
* In order to connect them, the "for" attribute of the lable element's always equal to the input element's id element.

#### The Select Element :::

* This element defines a drop-down list.
* Inside the select element option element defines option that can be selected from the drop down list.
* By default, the first element in the drop down list is selected., to defined pre-selected value we have to specify the selected attribute in option element.

```
<label for="vaccins">Number of Vaccins : </label>
<select id="vaccins" name="vaccins">
	<option value="one">One</option>
	<option value="two" selected>Two</option>
	<option value="three">Three</option>
</select>
```

#### The Textarea Element :::

* Defines multi-line input field.
* The rows and cols defines visible height and width for textarea respectively.

```
<testarea name="comments" rows=10 cols=10>
	Comment 1, Comment2, Comment3
</textarea>
```

#### The Button Element :::

* Defines the clickable button

```
<button type="button">
	Click Me!
</button>
```

#### The Fieldset and Legend Element :::

* Fieldset element is used to group related data in the form
* Legend element defines the caption for the fieldset element.

```
<form>
	<fieldset>
		<legend>Login</legend>
		<label for="email-address">Email :<label>
		<input type="email" id="email-address" value="abc@gmail.com"/>
		<input type="submit" value="Submit"/>
	</fieldset>
</form>
```

#### The Datalist Element :::
* This element defines pre-defined options for an input element.
* User will see drop down list of pre-defined options as per they input data.
* The list attribute of the input element must match with the id element of the datalist element.

```
<form>
	<label for="cars">Select Car :<label>
	<input list="cars">
	<datalist id="cars">
		<option value="BMW">
		<option value="Squada">
		<option value="Jeep">
		<option value="Mahindra-Thar">
	</datalist>
</form>
```

#### HTML Input Attributes
1. value: specifies initial value of the input field.
2. readonly: specifies that the input field is read only., the readonly input cannot be modified. the value of readonly input will sent on the submitting time.
3. disable: specifies that input field is disable i.e. unusable and unclickable., the value of disable will not be sent on submitting time.
4. size: specifies visible width of an input field in characters.
5. maxlength: specifies maximum characters allow in input field.
6. min and max attribute: specifies minimum and maximum value for an input field., the min and max attributes will work with number, range, date, date-time-local, month, time and week.
7. multiple: attribute allows user to enter more than one value. applicable for email and file type inputs.
8. pattern: attribute helps to set regex that input field's value will be checked while submitting the form, the pattern attribute will work for text, date, search, url, tel, email and password.
9. placeholder: attribute helps by giving short hint to user to enter the data.
10. required: attribute specifies that input field must be filled before submitting the form.
11. step: specifies legal number of intervals for input field, if step="2" legal numbers could be -2, 0, 2, 4 etc., the step attribute work with number, range, month, time, week and datetime-local.
12. autofocus: specifies that input field should automatically focused when the page loads.
13. height and width: specifies height and width for image input type.
14. list: the input list attribute refers to the datalist which containes pre-defined options for an input element.
15. autoconmplete: specifies whether a input field have autocomplete on or off.

### React Form Library
* It is a small library which helps to deal with forms in react.
* Manage form data, Submit form data, Enforce validation, Provide visual feedback are the reasons to use the react-hook form.
* All above things we can achieve through simple react form but react-hook-form gives us simplicity, scalability and performant way to manage even most complex forms.
* The react-hook-form gives us the **useForm** hook which helps us to deal with forms related operations.
```
import { useForm } from "react-hook-form";
```
* This useForm hook provides us the functions like register, handleSubmit, watch formState etc., which helps us to perform validation, form submission, error handleing etc.
```
 const { register, handleSubmit, watch, formState:{ errors } }  =  useForm();
```

#### Register :::
* Register function helps us to make input value available to the hook.
* This helps to give value for validation and submission.
* Each field should have name as a key for registration process.
```
  <label>First Name</label>
  <input {...register("firstName")}  />
```
* Props - onChange, onBlur, ref, name

#### Apply Validation :::
* require
If true, The input must have value before submitting the form.
```
<input
  {...register("test",  {
  required:  'error message'
  })}
/>
```
* min
The minimum value accept for the input.
```
<input
	type="number"
	{...register("test",  {
  min:  {
  value:  3,
  message:  'error message'
  }
  })}
/>
```
* max
The maximum value accept for the input.
```
<input
 type="number"
  {...register('test',  {
  max:  {
  value:  3,
  message:  'error message'
  }
  })}
/>
```
* minLength
The minimum length of input accept for input value.
```
<input
  {...register("test",  {
  minLength:  1
  })}
/>
```
* maxLength
The maximum length of the input accept for input value.
```
<input
  {...register("test",  {
  maxLength:  2
  })}
/>
```
* pattern
The regex pattern for the input
```
<input
  {...register("test",  {
  pattern:  {
  value:  /[A-Za-z]{3}/,
  message:  'error message'
  }
  })}
/>
```
* validate
We can pass callback function or object as a argument to validate.
This function will execute on it's own without depending on other validation rules including require.
```
<input
  {...register("test",  {
  validate:  value  => value ===  '1'  ||  'error message'
  })}
/>
```

* CodeSandBox - https://codesandbox.io/s/react-hook-form-hyf74k

* valueAsNumber
Returns number, If something goes wrong returns NaN.
valueAs process is happening before validation.
only applicable and support to number type input.
```
<input
	type="number"
  {...register("test",  {
  valueAsNumber:  true,
  })}
/>
```

* valueAsDate
return date object, If something goes wrong returns "Invalid Date".
valueAs process is happening before validation.
only applicable and support date type input.
```
<input
	type="date"
  {...register("test",  {
  valueAsDate:  true,
  })}
/>
```

* setValueAs
Returns input value by running through the function.
valueAs process happening before validation, setValueAs is ignored if either valueAsNumber or valueAsDate are true.
Only applies to text input
```
<input
	type="number"
  {...register("test",  {
  setValueAs:  v  =>  parseInt(v),
  })}
/>
```
* disabled
This field will make input value as undefined and make input control disabled.
Disable prop will by default omit validations.
```
<input
  {...register("test",  {
  disabled:  true
  })}
/>
```

* onChange
onChange function event to be invoked in the change event.
```
register('firstName',  {
  onChange:  (e)  =>  console.log(e)
})
```

* onBlur
onBlur function event to be invoked in the blur event.
```
register('firstName',  {
  onBlur:  (e)  =>  console.log(e)
})
```

* value
Set up value for registered.
This prop should be utilise inside useEffect or invoke once.
Each re-run will update or overwrite the input value which we have supplied.
```
register('firstName',  {  value:  'bill'  })
```

* shouldUnregister
Input will unregistered after unmount and default values will be removed as well.
```
<input
  {...register("test",  {
  shouldUnregister:  true,
  })}
/>
```

* deps
Validation will be triggered for the dependent inputs, it only limited to register API not trigger.
```
<input
  {...register("test",  {
  deps:  ['inputA',  'inputB'],
  })}
/>
```
