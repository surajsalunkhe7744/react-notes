### Why React Hooks
* To allow use of state, life cycle methods in functional component we need react hooks.
* Hooks allow us to use functionalities of class component like state

#### useState Hook
* Most common hook, which allows us to use state in functional component.
```
const [state, setState] = useState(initialState);
```
* In above statement state is the initial value, which we can directly use in functional component as other normal variable.
* The setState is a dispatcher function which helps to update the state value.
* The dispatcher function name should start with set and it should follow the declared state name, It is a convention for writing the useState dispatcher function name.

#### useEffect Hook
* useEffect is the second most hook used in react.
* Common cases where we use the useEffect hook - Use it as the initial render of the component (componentDidMount), Use it when component goes out of the scope (componentWillUnMount), to call or handle any peice of code for a perticular state change.
```
useEffect(() => {
//statements
}, [// dependecies])
```
* When using useEffect, we might need to return the cleanup function to remove event listeners or terminate the socket connection, among other things. The cleanup for the event listener is shown in the excerpt below.
```
const [width, setWidth] = useState(null);

useEffect(() => {
function watchWidth() {
setWidth(window.innerWidth);
}

window.addEventListener("resize", watchWidth);

return function () {
window.removeEventListener("resize", watchWidth);
}
}, []);
```
* If we want to use async function in useEffect we can't write it as follows
```
const [user, setUser] = useState(null);

useEffect(async () => {
	const data = await getData();
	setUser(data);
}, []);
```
UseEffect's intended return value of nothing is not what an async function, which returns a promise, would actually provide.
We can create a method inside the useEffect that will have async await functionality to prevent this.
```
const [user, setUser] = useState(null);

useEffect(() => {
const data = async () => {
await getData();
}
setUser(data);
}, []);
```
Alternately, a function outside the useEffect may be declared and called.
```
const [user, setUser] = useState(null);

const userData = async () => {
const data = await getData();
setUser(data);
}

useEffect(() => {
userData();
}, [])
```


#### useContext Hook
* This state allows us to pass the state of one component to any other component directly.
* Prop-Drilling: If you have to pass data from one component o the another component and you are pass the data through the components towords the target component is the Prop-Drilling., but using useContext hook we can pass data from base component to the target component directly.
* createContext: It return's context object, We can pass default values to them while declaring the createContext but that value will never change. but if still we want to update it we will wrap that value inside the state by which we can achieve the dynamic effect.
* Provider: createContext returns a context object which gives us Provider and this helps us to give value to the component for this we have to wrap the component with Provider.
* CodeSandBox - https://codesandbox.io/s/exa-usecontext-g5pt7y


#### useReduce Hook :::
* useReduce is the alternative for useState, but useReduce is prefered to useState when you have complex state logic or the next state is depends on the previous state.
```
const [state, dispatch] = useReducer(reducer, initialArg, init);
```
* useReducer returns the exact two values first is the state and second is the dispatcher.
* dispatcher will call the reducer which will perform further execution and return the new state. We generally pass action type and payload in a object while calling the dispatcher.
* CodeSandBox - https://codesandbox.io/s/exa-usereducer-tt7ts4

#### useCallback Hook :::
* useCallback function cached the function and it will only run when the one of it's dependecy will update.
* useCallback function helps to improve the performance by avoiding unwanted re-renders.
* Please refer following CodeSandBox link to see unwanted re-renders
https://codesandbox.io/s/exa-unwanted-renders-62kmvs
In above CodeSandBox example we have specified Button, Counter, Title components which are called inside the ParentComponent.
While making those component we have specified logs in each component which will help us to check that the component is re-rendered or not.
ParentComponent have two states age and salary and handlers to update their states.
After running the application, In console we can see the consoles for each component, If we update the age this leads to re-render the title component and also re-render for the salary part which is unwanted.
useCallback helps with avoid this unwanted renders and increase the application performance.
* What useCallback is doing is caching the function i.e. in javascript function is a type of object and each object have it's reference id so when our program is running again it will make another reference id for that function which is causing the re-renders and useCallback refers to the same reference id and it will update only when the one of useCallback dependency will be changed.
* CodeSandBox example for same code but with using useCallback
https://codesandbox.io/s/exa-usecallback-phwlw3
By performing steps mention in above you can see the difference of log statements.
In this CodeSandBox example I have used memo function which will re-render the component only when the state or the prop is changed.
* ref - https://www.knowledgehut.com/blog/web-development/all-about-react-usecallback

#### useMemo Hook :::
* useMemo hook will only re-compute the catched value when the one of the dependency should changed.
* CodeSandBox - https://codesandbox.io/s/exa-usememo-yfjslm
